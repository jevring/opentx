--[[
Copyright (c) 2016, Markus Jevring
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--]]
--[[
Telemetry script that calculates the number of cells and displays
the indovidual cell voltage. Also generates autible alarms when 
the cell voltage reaches below a certain threshold.

I created this as there was no way of getting the "Custom scripts" LUA
section working with EU LBT firmware. As a result, I needed a telemetry
screen AND audible warnings for voltage.

mute.wav and unmute.wav courtesy of http://winbox.open-tx.org/taranis-sounds-beta1/index.php
Mute icon (mute.png) made by Egor Rumyantsev (http://www.flaticon.com/authors/egor-rumyantsev) from http://www.flaticon.com is licensed by CC 3.0 BY (http://creativecommons.org/licenses/by/3.0/)

Author: Markus Jevring <markus@jevring.net>
--]]

-- CONFIGURATION
local voltageSource = "VFAS"
local warnCellVoltageLimit = 3.5
local criticalCellVoltageLimit = 3.3
local lowVoltageWarnIntervalInSeconds = 20
local lowVoltageCriticalIntervalInSeconds = 5
local numberOfRequiredVoltageSamples = 35
local muteSwitch = "sa"


-- STATE
local startTime = 0
local lastWarnTime = 0
local lastCriticalTime = 0
local cells = 0
local totalVoltage = 0.0
local averageCellVoltage = 0.0
local batteryFound = false
local batteryFoundTimestamp = 0
local now = 0
-- Smooth the input voltage. Sample once per 100ms, then average over the last 25 samples.
-- Keep a cyclic buffer of measurements
local voltageMeasurements = {}
local voltageMeasurementCurrentIndex = 1
local voltageMeasurementLastSampleTimestamp = 0
local voltageMeasurementsInitialized = false
local rssi = 0
local mute = false

-- DATA
local timerTriggers = {"off", "abs", "stk", "stk%", "sw/!sw", "m_sw/!m_sw"}


-- FUNCTIONS
local function init() 
	startTime = getTime()
end

local function background()
	fetchData()
end

-- Screen is 212 x 64
function run(keyEvent) 
	fetchData()
	lcd.clear()
	lcd.drawScreenTitle("mj_telem.lua - w: " .. warnCellVoltageLimit .. "V - c: " .. criticalCellVoltageLimit .. "V - m: " .. muteSwitch, 1, 1)
	if (batteryFound and rssi > 0) then
		if (averageCellVoltage < warnCellVoltageLimit) then
			lcd.drawText(5, 10, round(averageCellVoltage, 2), MIDSIZE + INVERS + BLINK)
			if (averageCellVoltage < criticalCellVoltageLimit) then
				lcd.drawText(40, 10, "Volts per cell (LAND!)", MIDSIZE + INVERS + BLINK)
			else
				lcd.drawText(40, 10, "Volts per cell (LOW!)", MIDSIZE + INVERS + BLINK)
			end
		else 
			lcd.drawText(5, 10, round(averageCellVoltage, 2), MIDSIZE)
			lcd.drawText(40, 10, "Volts per cell", MIDSIZE)
		end
		lcd.drawText(5, 22, round(totalVoltage, 2), MIDSIZE)
		lcd.drawText(40, 22, "Volts total", MIDSIZE)
		lcd.drawText(5, 34, cells, MIDSIZE)
		lcd.drawText(40, 34, "Cells", MIDSIZE)
	else
		lcd.drawText(5, 10, "No telemetry", DBLSIZE)
	end	
	lcd.drawPixmap(5, 53, "/SCRIPTS/BMP/antenna.bmp")
	lcd.drawText(15, 50, getValue("RSSI") .. " dB", MIDSIZE)
	
	if (mute) then
		lcd.drawPixmap(194, 8, "/SCRIPTS/BMP/mute.bmp")
	end
	
	-- Timer 0
	local timer0 = model.getTimer(0)
	lcd.drawText(90, 42, "Timer 1 " .. timerTriggers[timer0.mode + 1])
	lcd.drawTimer(90, 50, timer0.value, MIDSIZE + TIMEHOUR)
	
	-- Timer 1
	local timer1 = model.getTimer(1)
	lcd.drawText(150, 42, "Timer 2 " .. timerTriggers[timer1.mode + 1])
	lcd.drawTimer(150, 50, timer1.value, MIDSIZE + TIMEHOUR)
end

function fetchData()
	now = getTime()
	-- check the RSSI here, to avoid announcing when we lose the signal, 
	-- as the voltage has a delay
	rssi = getValue("RSSI")
	checkMuteSwitch()
	checkCellVoltageAndAlert()
end

function checkMuteSwitch()
	local muteValue = getValue(muteSwitch)
	if (muteValue == 1024) then
		if (not mute) then
			playFile("/SOUNDS/mute.wav")
		end
		mute = true
	else
		if (mute) then
			playFile("/SOUNDS/unmute.wav")
		end
		mute = false
	end
end

function checkCellVoltageAndAlert()
	calculateBatteryValues()
	if (not mute and
		batteryFound and 
		rssi > 0) then
		if (averageCellVoltage < warnCellVoltageLimit and 
			secondsSince(lastWarnTime) > lowVoltageWarnIntervalInSeconds and 
			secondsSince(batteryFoundTimestamp) > lowVoltageWarnIntervalInSeconds * 2 and
			secondsSince(startTime) > lowVoltageWarnIntervalInSeconds * 2) then
			playNumber(averageCellVoltage * 10, 1, PREC1) 
			lastWarnTime = getTime()
		end
		if (averageCellVoltage < criticalCellVoltageLimit and 
			secondsSince(lastCriticalTime) > lowVoltageCriticalIntervalInSeconds and 
			secondsSince(batteryFoundTimestamp) > lowVoltageCriticalIntervalInSeconds * 2 and
			secondsSince(startTime) > lowVoltageCriticalIntervalInSeconds * 2) then
			playFile("/SOUNDS/en/SYSTEM/lowbatt.wav") 
			lastCriticalTime = getTime()
		end
	end
end

function calculateBatteryValues()
	calculateAverageTotalVoltage()
	if (totalVoltage > 1) then
		--[[ 
		Dividing the cell voltage with 4.25 and taking ceil more-or-less accurately
		reflects the number of cells.
		ceil(21.00 / 4.25) = 5, which is the max voltage for a 5S pack
		ceil(16.80 / 4.25) = 4, which is the max voltage for a 4S pack
		ceil(12.75 / 4.25) = 4, which is the min voltage for a 4S pack (almost)
		ceil(12.60 / 4.25) = 3, which is the max voltage for a 3S pack
		ceil( 9.00 / 4.25) = 3, which is the min voltage for a 3S pack
		If you were to drain a 4S pack down to 3.0V per cell, it would 
		read as a 3S, so it's not flawless. Hopefully you'll stop 
		discharging before you get quite that far down, however =)
		
		Given that cell count is almost impossible to sense in some other
		way, however, this is what we've got.
		--]]	
		cells = math.ceil(totalVoltage / 4.25)
		averageCellVoltage = totalVoltage / cells
		if (cells > 1 and not batteryFound) then
			-- Avoid any early initialization issues
			
			-- Only set this if we didn't have a battery already, 
			-- as we'd otherwise end up setting it with each iteration,
			-- making it useless.
			-- We want this to represent the time we last found the battery, 
			-- i.e. when we powered on or when the battery was connected.
			-- This is to avoid acting on things immediately as they are connected
			batteryFoundTimestamp = now
			batteryFound = true
		end
	else
		batteryFound = false
		batteryFoundTimestamp = 0
	end
end

function calculateAverageTotalVoltage()
	-- sample once per 100ms
	-- time unit is "10ms"
	if (now - voltageMeasurementLastSampleTimestamp > 10) then
		local voltageMeasurement = getValue(voltageSource)
		voltageMeasurementLastSampleTimestamp = now
		voltageMeasurements[voltageMeasurementCurrentIndex] = voltageMeasurement 
		voltageMeasurementCurrentIndex = voltageMeasurementCurrentIndex + 1
		if (voltageMeasurementCurrentIndex > numberOfRequiredVoltageSamples) then
			voltageMeasurementCurrentIndex = 1
		end	
		
		if (not voltageMeasurementsInitialized) then
			-- We can't initialize this at the start, since that would skew the values.
			-- Even though initializing it here costs one more boolean check, 
			-- it's better than costing a boolean check for every value we sum up.
		
			-- lua "arrays" start their index at 1.
			-- they're actually some kind of proto-hash table, which is aweful, 
			-- but it's the only thing we have to work with...
			for i=1, numberOfRequiredVoltageSamples do
				voltageMeasurements[i] = voltageMeasurement
			end
			voltageMeasurementsInitialized = true
		end
		
		local sum = 0
		for i=1, numberOfRequiredVoltageSamples do 
			sum = sum +	voltageMeasurements[i]
		end
		totalVoltage = sum / numberOfRequiredVoltageSamples
	end
end

function secondsSince(timestamp) 
	return (now - timestamp) / 100
end

-- https://raw.githubusercontent.com/opentx/lua-reference-guide/master/lcd/drawNumber-example.lua
function round(num, decimals)
  local mult = 10^(decimals or 0)
  return math.floor(num * mult + 0.5) / mult
end


return  {init=init,run=run,background=background}