--[[
Copyright (c) 2016, Markus Jevring
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--]]

--[[
Calculates the number of cells currently being used and the average cell voltage.
Author: Markus Jevring <markus@jevring.net>
--]]
local inputs = { {"Voltage source", SOURCE} }
local outputs = { "VCel", "Cells"} 
local function run()
	local totalVoltage = getValue("VFAS")
	--[[ 
		Dividing the cell voltage with 4.25 and taking ceil more-or-less accurately
		reflects the number of cells.
		ceil(21.00 / 4.25) = 5, which is the max voltage for a 5S pack
		ceil(16.80 / 4.25) = 4, which is the max voltage for a 4S pack
		ceil(12.75 / 4.25) = 4, which is the min voltage for a 4S pack (almost)
		ceil(12.60 / 4.25) = 3, which is the max voltage for a 3S pack
		ceil( 9.00 / 4.25) = 3, which is the min voltage for a 3S pack
		If you were to drain a 4S pack down to 3.0V per cell, it would 
		read as a 3S, so it's not flawless. Hopefully you'll stop 
		discharging before you get quite that far down, however =)
		
		Given that cell count is almost impossible to sense in some other
		way, however, this is what we've got.
	--]]	
	local cells = math.ceil(totalVoltage / 4.25)
	local averageCellVoltage = totalVoltage / cells
	-- This must match 'outputs' above, in order and semantics
	return cells, averageCellVoltage;
end

return {run=run, input=inputs, output=outputs}